import asyncio
from pathlib import Path
import re
from typing import Any

import pandas as pd
from nacsos_data.db import get_engine_async
from nacsos_data.models.items import AcademicItemModel
from nacsos_data.models.items.academic import AcademicAuthorModel
from nacsos_data.util.academic.importer import import_academic_items
from pyarrow import feather

PROJECT_ID = 'cc72b5f6-9ab2-40c5-8df9-8bea9c80861d'
IMPORT_ID = '9ec9c4b3-8b8a-4f96-8859-10a663489cfb'
MISSING_IMPORT_ID = '9cacd6c5-48a5-4037-bac3-1c4c35c7f661'

DOI_REM = re.compile(r'.*doi\.org/')
INT = re.compile(r'[^0-9]')


def clean(d: dict[str, Any]) -> dict[str, Any]:
    return {
        k: v
        for k, v in d.items()
        if v is not None
    }


def s2i(s):
    if s is None:
        return None
    if type(s) is str:
        s = INT.sub('', s)
        if len(s) < 4:
            return None
        return int(s)
    return int(s)


def get_doi(row: pd.Series) -> str | None:
    doi = row.get('DOI') or row.get('doi') or None
    if doi is None or len(doi) < 5:
        return None
    doi = DOI_REM.sub(doi, '')
    if 'http' in doi or doi[:3] != '10.':
        return None
    return doi


def get_keywords(row: pd.Series) -> list[str] | None:
    if row['keywords'] is not None and type(row['keywords']) is str and len(row['keywords']) > 0:
        return row['keywords'].split()
    return None


def get_authors(row: pd.Series) -> list[AcademicAuthorModel] | None:
    def trans(a: str) -> str:
        a = a.replace('{', '')
        a = a.replace('}', '')
        a = a.replace('\\', '')
        a = a.replace("'", '')
        a = a.replace('"', '')
        ps = a.split(',')
        if len(ps) > 1:
            return f'{ps[1].strip()} {ps[0].strip()}'
        return ps[0].strip()

    au = row['author']
    if au is not None and len(au) > 5:
        aus = au.split(' and ')
        return [AcademicAuthorModel(name=trans(a_)) for a_ in aus if len(a_.strip()) > 0]
    return None


def get_yr(row: pd.Series) -> int | None:
    return s2i(row['year'])


def get_source(row: pd.Series) -> str | None:
    if row['booktitle'] is not None and type(row['booktitle']) is str and len(row['booktitle']) > 0:
        return row['booktitle']
    if row['journaltitle'] is not None and type(row['journaltitle']) is str and len(row['journaltitle']) > 0:
        return row['journaltitle']
    return None


def produce_missing_items():
    path = Path('data/ipcc_ar6')
    files = list(path.glob('*_missed.feather'))
    for i, file in enumerate(files):
        print(f'{i}/{len(files)}: {file}')
        missed = feather.read_feather(str(file))
        yield from [
            AcademicItemModel(
                project_id=PROJECT_ID,
                text=row['abstract'],
                title=row['title'][:400] if row['title'] is not None else None,
                publication_year=get_yr(row),
                source=get_source(row),
                keywords=get_keywords(row),
                authors=get_authors(row),
                doi=get_doi(row),
                meta={
                    'source': f"AR6-{row['report']}-Ch{row['chapter_number']}",
                    'matched': 'false',
                    'ipcc': clean(row.to_dict())
                }
            )
            for _, row in missed.iterrows()
        ]


def produce_items():
    skip = {

    }

    path = Path('data/ipcc_ar6')
    files = list(path.glob('*.jsonl'))
    for i, file in enumerate(files):
        if str(file) in skip:
            print(file, '- skip')
            continue
        print(f'{i}/{len(files)}: {file}')
        with open(file) as f:
            yield from [AcademicItemModel.model_validate_json(line) for line in f]


async def main():
    db_engine = get_engine_async(conf_file='../../nacsos-core/config/remote.env')

    # await import_academic_items(
    #     items=produce_items(),
    #     project_id=PROJECT_ID,
    #     db_engine=db_engine,
    #     import_name=None,
    #     import_id=IMPORT_ID,
    #     user_id=None,
    #     description=None,
    #     dry_run=False,
    #     trust_new_authors=True,
    #     trust_new_keywords=False)

    await import_academic_items(
        items=produce_missing_items(),
        project_id=PROJECT_ID,
        db_engine=db_engine,
        import_name=None,
        import_id=MISSING_IMPORT_ID,
        user_id=None,
        description=None,
        dry_run=False,
        trust_new_authors=False,
        trust_new_keywords=False)


if __name__ == '__main__':
    asyncio.run(main())
