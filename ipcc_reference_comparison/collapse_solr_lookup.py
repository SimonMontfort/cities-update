import json
from pathlib import Path
from tqdm import tqdm
import pandas as pd

cache = []
for fn in tqdm(Path().glob('../../data/ipcc_ar6/*.jsonl')):
    with open(fn) as f:
        for line in f:
            wrk = json.loads(line)
            cache.append({
                'id': wrk['openalex_id'],
                'doi': wrk['doi'],
                'text': wrk['text'],
                'title': wrk['title'],
                'py': wrk['publication_year'],
                'source': wrk['source'],
                'authors': wrk['authors'],
                'ipcc_ref': wrk['meta']['ipcc_ref'],
                'matched': wrk['meta']['matched'],
                'ipcc_src': wrk['meta']['source'],
                'ipcc_doi': wrk['meta']['ipcc'].get('doi'),
                'ipcc_tit': wrk['meta']['ipcc'].get('title'),
                'ipcc_pub': wrk['meta']['ipcc'].get('publisher'),
                'ipcc_py': wrk['meta']['ipcc'].get('year')
            })
pd.DataFrame(cache).to_feather('../../data/ipcc_ar6.feather')

(
    pd
    .concat([pd.read_feather(fn) for fn in Path().glob('../../data/ipcc_ar6/*.feather')])
    .reset_index()
    .to_feather('../../data/ipcc_ar6_missing.feather')
)
