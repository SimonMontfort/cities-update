from logging import Logger
import typer
from typing import Optional, NamedTuple
import pandas as pd
import requests
from bibtexparser import parse_string
from bibtexparser.middlewares import LatexDecodingMiddleware
from pathlib import Path

from shared.log import Verbosity, get_logger


class Chapter(NamedTuple):
    chapter: int
    title: str
    bib_url: str


AR6_WG1_chapters = [
    Chapter(1, 'Framing, Context and Methods',
            'https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_References_Chapter01.bib'),
    Chapter(2, 'Changing State of the Climate System',
            'https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_References_Chapter02.bib'),
    Chapter(3, 'Human Influence on the Climate System',
            'https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_References_Chapter03.bib'),
    Chapter(4, 'Future Global Climate',
            'https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_References_Chapter04.bib'),
    Chapter(5, 'Global Carbon and other Biogeochemical Cycles and Feedbacks',
            'https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_References_Chapter05.bib'),
    Chapter(6, 'Short-lived Climate Forcers',
            'https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_References_Chapter06.bib'),
    Chapter(7, 'The Earth’s Energy Budget, Climate Feedbacks, and Climate Sensitivity',
            'https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_References_Chapter07.bib'),
    Chapter(8, 'Water Cycle Changes',
            'https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_References_Chapter08.bib'),
    Chapter(9, 'Ocean, Cryosphere and Sea Level Change',
            'https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_References_Chapter09.bib'),
    Chapter(10, 'Linking Global to Regional Climate Change',
            'https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_References_Chapter10.bib'),
    Chapter(11, 'Weather and Climate Extreme Events in a Changing Climate',
            'https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_References_Chapter11.bib'),
    Chapter(12, 'Climate Change Information for Regional Impact and for Risk Assessment',
            'https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_References_Chapter12.bib')
]
AR6_WG2_chapters = [
    Chapter(1, 'Point of Departure and Key Concepts',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter01.bib'),
    Chapter(2, 'Terrestrial and Freshwater Ecosystems and their Services',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter02.bib'),
    Chapter(3, 'Oceans and Coastal Ecosystems and their Services',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter03.bib'),
    Chapter(4, 'Water',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter04.bib'),
    Chapter(5, 'Food, fibre, and other ecosystem products',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter05.bib'),
    Chapter(6, 'Cities, settlements and key infrastructure',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter06.bib'),
    Chapter(7, 'Health, wellbeing and the changing structure of communities',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter07.bib'),
    Chapter(8, 'Poverty, livelihoods and sustainable development',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter08.bib'),
    Chapter(9, 'Africa',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter09.bib'),
    Chapter(10, 'Asia',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter10.bib'),
    Chapter(11, 'Australasia',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter11.bib'),
    Chapter(12, 'Central and South America',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter12.bib'),
    Chapter(13, 'Europe',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter13.bib'),
    Chapter(14, 'North America',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter14.bib'),
    Chapter(15, 'Small Islands',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter15.bib'),
    Chapter(16, 'Key risks across sectors and regions',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter16.bib'),
    Chapter(17, 'Decision-making options for managing risk',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter17.bib'),
    Chapter(18, 'Climate resilient development pathways',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter18.bib'),
]
AR6_WG2_ccps = [
    Chapter(1, 'Biodiversity hotspots (land, coasts and oceans)',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_CCP1.bib'),
    Chapter(2, 'Cities and settlements by the sea',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_CCP2.bib'),
    Chapter(3, 'Deserts, semi-arid areas, and desertification',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_CCP3.bib'),
    Chapter(4, 'Mediterranean region',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_CCP4.bib'),
    Chapter(5, 'Mountains',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_CCP5.bib'),
    Chapter(6, 'Polar regions',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_CCP6.bib'),
    Chapter(7, 'Tropical forests',
            'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_CCP7.bib')
]

AR6_WG3_chapters = [
    Chapter(1, 'Introduction and Framing',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter01.bib'),
    Chapter(2, 'Emissions trends and drivers',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter02.bib'),
    Chapter(3, 'Mitigation pathways compatible with long-term goals',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter03.bib'),
    Chapter(4, 'Mitigation and development pathways in the near- to mid-term',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter04.bib'),
    Chapter(5, 'Demand, services and social aspects of mitigation',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter05.bib'),
    Chapter(6, 'Energy systems',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter06.bib'),
    Chapter(7, 'Agriculture, Forestry, and Other Land Uses (AFOLU)',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter07.bib'),
    Chapter(8, 'Urban systems and other settlements',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter08.bib'),
    Chapter(9, 'Buildings',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter09.bib'),
    Chapter(10, 'Transport',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter10.bib'),
    Chapter(11, 'Industry',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter11.bib'),
    Chapter(12, 'Cross sectoral perspectives',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter12.bib'),
    Chapter(13, 'National and sub-national policies and institutions',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter13.bib'),
    Chapter(14, 'International cooperation',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter14.bib'),
    Chapter(15, 'Investment and finance',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter15.bib'),
    Chapter(16, 'Innovation, technology development and transfer',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter16.bib'),
    Chapter(17, 'Accelerating the transition in the context of sustainable development',
            'https://www.ipcc.ch/report/ar6/wg3/downloads/report/IPCC_AR6_WGIII_References_Chapter17.bib')
]

AR5_WG1_bibliography = 'https://archive.ipcc.ch/report/ar5/wg1/citation/WGIAR5_Citations_FinalRev1.enl'
AR5_WG2_bibliography = 'https://archive.ipcc.ch/report/ar5/wg2/docs/All_Citations.ris'
AR5_WG3_bibliography = 'https://archive.ipcc.ch/report/ar5/wg3/docs/IPCC_WGIII_TSU_AR5_Chapter_Citations_Final.rdf'


# To generate lists above:
# 1. Copy list of chapters from (e.g.)
#    https://www.ipcc.ch/report/ar6/wg2/downloads
# 2. Run (a modified) version of the following code snippet
#
# for c in ch.split('\n'):
#     s = c[8:].split(': ')
#     ci = int(s[0])
#     cn = s[1].strip()
#     u = f'https://www.ipcc.ch/report/ar6/wg2/downloads/report/IPCC_AR6_WGII_References_Chapter{ci:02d}.bib'
#     print(f"Chapter({ci}, '{cn}',\n '{u}'),")


def fetch_bibfile(url: str,
                  bibfilename: Path,
                  logger: Logger) -> str:
    if not bibfilename.exists():
        logger.debug(f'Bibfile "{bibfilename}" does not exist, downloading it.')

        # Get bibtex file from the IPCC website
        request = requests.get(url)
        response = request.content.decode()

        # inject missing line breaks
        bibtex = response.replace('}@', '}\n@')

        with open(bibfilename, 'w') as bibfile:
            bibfile.write(bibtex)
        return bibtex

    logger.debug(f'Bibfile already exists, reading from file {bibfilename}.')
    with open(bibfilename, 'r') as bibfile:
        return bibfile.read()


def fetch_chapters(chapters: list[Chapter],
                   wg: str,
                   bibfiles: Path,
                   logger: Logger) -> list[dict[str, str | int]]:
    references = []

    for chapter in chapters:
        logger.info(f'Fetching bibliography for Chapter {chapter.chapter}—{chapter.title}')

        bibfilename = bibfiles / wg / f'chapter_{chapter.chapter:02d}.bib'
        bibfilename.parent.mkdir(parents=True, exist_ok=True)
        bibtex = fetch_bibfile(chapter.bib_url, bibfilename=bibfilename, logger=logger)

        # parse the bibtex file and translate into list of dicts
        library = parse_string(bibtex, append_middleware=[LatexDecodingMiddleware()])
        logger.info(f'  > Found {len(library.entries)} references.')
        library_dicts = [
            {
                'report': wg,
                'chapter_number': chapter.chapter,
                'chapter_title': chapter.title,
                **{field.key: field.value for field in entry.fields}
            }
            for entry in library.entries
        ]

        references += library_dicts
    return references


def main(wg1: bool = True,
         wg2: bool = True,
         wg2_ccp: bool = True,
         wg3: bool = True,
         output: Path = Path('data/raw/IPCC/AR6_references.feather'),
         bibfiles: Path = Path('data/raw/IPCC/AR6_bibfiles'),
         logfile: Optional[Path] = None,
         verbose: Verbosity = 2):
    logger = get_logger('refs', verbosity=verbose, logfile=logfile)

    # Ensure path to file exists
    output.parent.mkdir(parents=True, exist_ok=True)

    references = []

    if wg1:
        references += fetch_chapters(AR6_WG1_chapters, wg='WG1', bibfiles=bibfiles, logger=logger)
    if wg2:
        references += fetch_chapters(AR6_WG2_chapters, wg='WG2', bibfiles=bibfiles, logger=logger)
    if wg3:
        references += fetch_chapters(AR6_WG3_chapters, wg='WG3', bibfiles=bibfiles, logger=logger)
    if wg2_ccp:
        references += fetch_chapters(AR6_WG2_ccps, wg='CCP', bibfiles=bibfiles, logger=logger)

    logger.info(f'Found {len(references)} references overall.')
    logger.info(f'Writing to {output}.')

    df = pd.DataFrame(references)
    if output.suffix == '.csv':
        df.to_csv(output)
    elif output.suffix == '.feather':
        df.to_feather(output)
    else:
        raise NotImplementedError(f'Unsupported filetype "{output.suffix}"')


if __name__ == '__main__':
    typer.run(main)
