import uuid
import asyncio
from pathlib import Path
from random import shuffle

from pyarrow import feather
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from nacsos_data.db import get_engine_async
from nacsos_data.db.schemas import AcademicItem
from nacsos_data.db.crud.annotations import store_assignments
from nacsos_data.models.items import AcademicItemModel
from nacsos_data.models.annotations import AssignmentModel, AssignmentStatus

PROJECT_ID = 'cc72b5f6-9ab2-40c5-8df9-8bea9c80861d'
IMPORT_ID = '9ec9c4b3-8b8a-4f96-8859-10a663489cfb'
SCHEME_ID = 'c18429cb-cfbd-4ecf-830b-139d677a085f'

BATCH_1 = '5fa034c4-3c2d-490a-b6a0-1d20427c022f'
BATCH_2 = '48217382-506c-4357-bf0e-8eaf3c430eeb'
BATCH_3 = '6a3a3183-26d5-4c20-9781-2ca8a265ece0'

SIMON = '35d492f5-6855-440c-870f-7f3d44b6dc0e'
JAN = '7018e426-857e-4e2b-a800-9402c0532af2'
TIM = 'fd641232-bada-466e-9a3b-fb12038f5508'


def chapter_filter(refs: list[AcademicItemModel], rep, chap):
    key = f'AR6-{rep}-Ch{chap}'
    oa_ids = [r.openalex_id
              for r in refs
              if r.meta['source'] == key]
    print(f'[AR6-{rep}-Ch{chap}] I found {len(oa_ids):,} AR6 references ({len(set(oa_ids)):,} unique)')
    oa_ids = set(oa_ids)
    return oa_ids


async def main():
    path = Path('data/ipcc_ar6')
    db_engine = get_engine_async(conf_file='../../nacsos-core/config/remote.env')

    df_cities = feather.read_feather('data/cities.feather')
    city_ids = set(df_cities['id'])

    async def assign(items, scope_id):
        oa_ids = [r.openalex_id for r in items]
        print(f'I have {len(oa_ids):,} AR6 references ({len(set(oa_ids)):,} unique) '
              f'and {len(city_ids):,} city references')
        oa_ids = set(oa_ids)
        async with db_engine.session() as session:  # type: AsyncSession
            pool = oa_ids - city_ids
            pool = list(pool)
            shuffle(pool)
            pool = pool[:300]
            stmt = (select(AcademicItem.item_id, AcademicItem.openalex_id)
                    .where(AcademicItem.openalex_id.in_(pool)))

            items = (await session.execute(stmt)).mappings().all()

            id_lookup = {
                item['openalex_id']: item['item_id']
                for item in items
            }

            pool = [(p, id_lookup[p]) for p in pool if p in id_lookup]
            print(pool)
            for ids, user in [(pool[:50], SIMON), (pool[50:100], JAN), (pool[100:120], TIM)]:
                assignments = []
                for oa_id, item_id in ids:
                    assignments.append(AssignmentModel(assignment_id=uuid.uuid4(),
                                                       assignment_scope_id=scope_id,
                                                       user_id=user,
                                                       item_id=item_id,
                                                       annotation_scheme_id=SCHEME_ID,
                                                       status=AssignmentStatus.OPEN))
                await store_assignments(assignments, db_engine)

    print('wg2')
    with open(path / 'WG2_Ch06.jsonl') as f:
        wg2ch6 = [AcademicItemModel.model_validate_json(line) for line in f]
    await assign(wg2ch6, BATCH_1)

    print('ccp')
    with open(path / 'CCP_Ch02.jsonl') as f:
        ccpch2 = [AcademicItemModel.model_validate_json(line) for line in f]
    await assign(ccpch2, BATCH_2)

    print('wg3')
    with open(path / 'WG3_Ch08.jsonl') as f:
        wg3ch8 = [AcademicItemModel.model_validate_json(line) for line in f]
    await assign(wg3ch8, BATCH_3)


if __name__ == '__main__':
    asyncio.run(main())
