import asyncio
import logging
import httpx

import pandas as pd
from nacsos_data.db import get_engine_async
from nacsos_data.models.openalex.solr import WorkSolr
from nacsos_data.util.academic.importer import import_academic_items
from nacsos_data.util.academic.openalex import translate_doc, translate_work

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.DEBUG)
logger = logging.getLogger('upload')
logging.getLogger('httpcore').setLevel(logging.WARNING)
logging.getLogger('httpx').setLevel(logging.WARNING)

PROJECT_ID = 'cc72b5f6-9ab2-40c5-8df9-8bea9c80861d'
IMPORT_ID = 'b162b444-780d-4646-bf54-e45220bfd7ab'

BATCH_SIZE = 100


def batched(df):
    buffer = []
    for _, row in df.iterrows():
        buffer.append(row)
        if len(buffer) > BATCH_SIZE:
            yield buffer
            buffer = []
    yield buffer


def produce_items():
    df = pd.read_csv('data/oa_sub_key_words.csv')
    logger.info(f'Opened file with {df.shape}')
    for i, batch in enumerate(batched(df)):
        logger.debug(f'Fetching data from batch {i:,}')
        batch_ids = [row['id'] for row in batch]
        req = httpx.get(f'http://srv-mcc-apsis-rechner:8983/solr/openalex/get?ids={",".join(batch_ids)}')
        res = req.json()
        yield from [translate_work(translate_doc(WorkSolr.model_validate(r))) for r in res['response']['docs']]


async def main():
    db_engine = get_engine_async(conf_file='/home/tim/workspace/nacsos-core/config/remote.env')

    await import_academic_items(
        items=produce_items(),
        project_id=PROJECT_ID,
        db_engine=db_engine,
        import_name=None,
        import_id=IMPORT_ID,
        user_id=None,
        description=None,
        dry_run=True,
        trust_new_authors=True,
        trust_new_keywords=False)


if __name__ == '__main__':
    asyncio.run(main())
