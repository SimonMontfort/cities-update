import json

import pandas as pd
from tqdm import tqdm
from pathlib import Path
import httpx

old_climate = pd.read_feather('../../data/climate_change.feather').set_index('id')
old_ids_climate = set(old_climate.index.to_list())
print(f'{old_climate.shape[0]:,} in climate table; {len(old_ids_climate):,} unique ids')

df_climate = pd.read_feather('../../data/works_climate.feather').set_index('id')
ids_climate = set(df_climate.index.to_list())
print(f'{df_climate.shape[0]:,} in climate table; {len(ids_climate):,} unique ids')

mid_files = Path().glob('../../data/raw/openalex_merged_ids/*.csv.gz')
df_mids = pd.concat([pd.read_csv(fn, compression='gzip') for fn in mid_files])
ids_merged = set(df_mids['id'].unique())
print(f'{len(ids_merged):,} IDs were merged')

ids_gone = old_ids_climate - ids_climate - ids_merged
print(f'num missing: {len(ids_gone):,}')

with open('../../data/raw/missed_documents.jsonl', 'w') as f:
    for wid in tqdm(ids_gone):
        res = httpx.post('http://srv-mcc-apsis-rechner:8983/solr/openalex/select', data={
            'q': wid,
            'q.op': 'AND',
            'sort': 'id desc',
            'df': 'id',
            'fl': ','.join([
                'id', 'title', 'abstract', 'display_name',  # 'title_abstract',
                'publication_year', 'publication_date',
                'created_date', 'updated_date',
                'cited_by_count', 'type', 'doi', 'mag', 'pmid', 'pmcid',
                'is_oa', 'is_paratext', 'is_retracted',
                'locations', 'authorships', 'biblio', 'language'
            ]),
            'rows': 5
        }, timeout=60).json()

        docs = res['response']['docs']
        f.write(json.dumps({
            'n_total': res['response']['numFound'],
            'n_response': len(res['response']['docs']),
            'work': docs[0] if len(docs) > 0 else None
        }) + '\n')
