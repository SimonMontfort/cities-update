SELECT item.item_id,
       min(aiv.title)                                    as title,
       min(aiv.publication_year)                         as PY,
       min(aiv.doi)                                      as doi,
       min(aiv.openalex_id)                              as openalex_id,
       min(item.text)                                    as abstract,
       array_agg(jsonb_build_object('key', ann.key,
                                    'vb', ann.value_bool,
                                    'vi', ann.value_int,
                                    'vs', ann.value_str,
                                    'user', u.username)) as anno
FROM annotation ann
         LEFT JOIN assignment ass ON ann.assignment_id = ass.assignment_id
         LEFT JOIN item ON ass.item_id = item.item_id
         LEFT JOIN academic_item aiv on item.item_id = aiv.item_id
         LEFT JOIN "user" u on ann.user_id = u.user_id
WHERE ass.assignment_scope_id IN ('6a3a3183-26d5-4c20-9781-2ca8a265ece0',
                                  '48217382-506c-4357-bf0e-8eaf3c430eeb',
                                  '5fa034c4-3c2d-490a-b6a0-1d20427c022f')
GROUP BY item.item_id;


SELECT item.item_id,
       min(aiv.title)                                                                           as title,
       min(aiv.publication_year)                                                                as PY,
       min(aiv.doi)                                                                             as doi,
       min(aiv.openalex_id)                                                                     as openalex_id,
       min(item.text)                                                                           as abstract,
       array_agg(jsonb_build_object('key', ann.key, 'val', ann.value_bool, 'user', u.username)) as anno
FROM annotation ann
         LEFT JOIN assignment ass ON ann.assignment_id = ass.assignment_id
         LEFT JOIN item ON ass.item_id = item.item_id
         LEFT JOIN academic_item aiv on item.item_id = aiv.item_id
         LEFT JOIN "user" u on ann.user_id = u.user_id
WHERE ass.assignment_scope_id = '19f95449-9104-45e5-82d4-c877bd6a2e80'
GROUP BY item.item_id;


SELECT username, key, value_bool, COUNT(1) as cnt
FROM annotation a
         LEFT JOIN "user" on a.user_id = "user".user_id
WHERE annotation_scheme_id = 'f4bfb4f0-71eb-4cfc-b424-41f2be5517a2'
GROUP BY username, key, value_bool
order by  key, username, value_bool desc;

WITH total as (SELECT COUNT(DISTINCT item_id) as s
               FROM annotation
               WHERE annotation_scheme_id = 'f4bfb4f0-71eb-4cfc-b424-41f2be5517a2')
SELECT key, value_bool, COUNT(1) as cnt, count(1)::float / total.s as rel
FROM annotation a,total
WHERE annotation_scheme_id = 'f4bfb4f0-71eb-4cfc-b424-41f2be5517a2'
GROUP BY  key, value_bool, total.s
order by  key, value_bool desc, cnt desc;


SELECT COUNT(DISTINCT item_id) as s
FROM annotation
WHERE annotation_scheme_id = 'f4bfb4f0-71eb-4cfc-b424-41f2be5517a2';

SELECT  c, COUNT(1) as cnt
FROM (SELECT item_id, array_agg(key order by key) as c
      FROM
                   annotation a
         LEFT JOIN "user" on a.user_id = "user".user_id
WHERE annotation_scheme_id = 'f4bfb4f0-71eb-4cfc-b424-41f2be5517a2'
AND key in ('adaptation','mitigation','impacts') AND value_bool = false
GROUP BY item_id) a
group by c;
