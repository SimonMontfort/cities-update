from logging import Logger
import typer
from typing import Optional, Sequence
import pandas as pd
from pathlib import Path
from sqlalchemy import text, RowMapping
from sqlalchemy.orm import Session
from shared.log import Verbosity, get_logger
from shared.db import get_engine, DatabaseEngine


def doi_lookup(doi: str, engine: DatabaseEngine) -> Sequence[RowMapping]:
    with engine.session() as session:  # type: Session
        stmt = text('SELECT id, doi FROM openalex.works WHERE doi = :doi;')
        res = session.execute(stmt, {'doi': doi})
        return res.mappings().all()


def get_work(engine: DatabaseEngine,
             work_id: str | None = None,
             title: str | None = None,
             doi: str | None = None) -> Sequence[RowMapping]:
    with engine.session() as session:  # type: Session
        if work_id is not None:
            where = 'w.id = :wid'
        elif title is not None:
            where = 'w.title = :title'
        elif doi is not None:
            where = 'w.doi = :doi'
        else:
            raise AttributeError('Exactly one of the search fields has to be provided!')

        stmt = text(f'''
            SELECT w.*,
                   to_jsonb(array_agg(wa.*)) as authorships,
                   to_jsonb(array_agg(a.*))  as authors,
                   to_jsonb(array_agg(i.*))  as affiliations
            FROM openalex.works w
                     LEFT JOIN openalex.works_authorships wa on w.id = wa.work_id
                     LEFT JOIN openalex.authors a on wa.author_id = a.id
                     LEFT JOIN openalex.institutions i on wa.institutions = i.id
            WHERE {where}
            GROUP BY w.id;
        ''')
        res = session.execute(stmt, {'wid': work_id, 'title': title, 'doi': doi})
        return res.mappings().all()


def main(references: Path = Path('data/raw/IPCC/AR6_references.feather'),
         matches: Path = Path('data/raw/IPCC/bibfiles'),
         logfile: Optional[Path] = None,
         verbose: Verbosity = 2):
    logger = get_logger('refs', verbosity=verbose, logfile=logfile)
    engine = get_engine(debug=verbose > 2)

    logger.info(f'Trying to load list of references from {references}')

    if references.suffix == '.csv':
        df_refs = pd.read_csv(references)
    elif references.suffix == '.feather':
        df_refs = pd.read_feather(references)
    else:
        raise NotImplementedError(f'Unsupported filetype "{references.suffix}"')

    logger.debug(f'Loaded {df_refs.shape} references.')

    for ref_id, reference in df_refs.iterrows():
        candidates_doi = get_work(doi=reference['doi'], engine=engine)
        candidates_title = get_work(title=reference['title'], engine=engine)




    # (cdf
    # .groupby('doi')['IPCC chapter']
    # .nunique()
    # .to_frame('n')
    # .reset_index()
    # .merge(cdf)
    # .drop_duplicates('doi')
    # .sort_values('n', ascending=False)
    # .reset_index()
    # .head(20)[['title', 'author', 'n']]
    # )


if __name__ == '__main__':
    typer.run(main)
