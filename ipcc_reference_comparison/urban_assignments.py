import uuid
import asyncio
from pathlib import Path
from random import shuffle

import pandas as pd
from pyarrow import feather
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from nacsos_data.db import get_engine_async
from nacsos_data.db.schemas import AcademicItem
from nacsos_data.db.crud.annotations import store_assignments
from nacsos_data.models.items import AcademicItemModel
from nacsos_data.models.annotations import AssignmentModel, AssignmentStatus

PROJECT_ID = 'cc72b5f6-9ab2-40c5-8df9-8bea9c80861d'
SCHEME_ID = 'f4bfb4f0-71eb-4cfc-b424-41f2be5517a2'
SCOPE_ID = '19f95449-9104-45e5-82d4-c877bd6a2e80'

SIMON = '35d492f5-6855-440c-870f-7f3d44b6dc0e'
JAN = '7018e426-857e-4e2b-a800-9402c0532af2'
TIM = 'fd641232-bada-466e-9a3b-fb12038f5508'
MAX = 'b4c20ee5-e415-4ac8-8e9d-77770311e38c'
FELIX = '6f57074c-b145-4826-a20c-2aba8939758f'


async def main():
    db_engine = get_engine_async(conf_file='/home/tim/workspace/nacsos-core/config/remote.env')

    df_cities = feather.read_feather('data/cities.feather')
    city_ids = set(df_cities['id'])
    df_urban = pd.read_csv('data/oa_sub_key_words.csv')
    urban_ids = set([row['id'] for _, row in df_urban.iterrows()])

    id_pool = urban_ids - city_ids

    print(f'cities: {len(city_ids):,}, urban: {len(urban_ids):,}, pool: {len(id_pool):,}')

    async with db_engine.session() as session:  # type: AsyncSession
        pool = list(id_pool)
        shuffle(pool)
        pool = pool[:800]
        stmt = (select(AcademicItem.item_id, AcademicItem.openalex_id)
                .where(AcademicItem.openalex_id.in_(pool)))

        items = (await session.execute(stmt)).mappings().all()

        id_lookup = {
            item['openalex_id']: item['item_id']
            for item in items
        }

        pool = [(p, id_lookup[p]) for p in pool if p in id_lookup]
        print(pool)
        for ids, user in [(pool[:100], SIMON),
                          (pool[100:200], JAN),
                          (pool[200:300], TIM),
                          (pool[300:400], MAX),
                          (pool[400:500], FELIX)]:
            assignments = []
            for oa_id, item_id in ids:
                assignments.append(AssignmentModel(assignment_id=uuid.uuid4(),
                                                   assignment_scope_id=SCOPE_ID,
                                                   user_id=user,
                                                   item_id=item_id,
                                                   annotation_scheme_id=SCHEME_ID,
                                                   status=AssignmentStatus.OPEN))
            await store_assignments(assignments, db_engine)


if __name__ == '__main__':
    asyncio.run(main())
