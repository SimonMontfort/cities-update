import re
import logging
from multiprocessing import Pool
from pathlib import Path
from typing import Optional, Any

import typer
import httpx
import pandas as pd
import pyarrow.feather as feather

from nacsos_data.models.items import AcademicItemModel

from nacsos_data.models.openalex.solr import WorkSolr, Work
from nacsos_data.util.academic.openalex import translate_doc, translate_work

from shared.log import Verbosity, get_logger


def query(q: str, df: str, oae: str, op: str = 'AND', limit: int = 20):
    for _ in range(3):
        try:
            request = httpx.post(f'{oae}/select', data={
                'q': q,
                'q.op': op,
                'useParams': '',
                'rows': limit,
                'df': df,
                'indent': 'false'
            })
            response = request.json()
            return [
                translate_doc(WorkSolr.model_validate(d))
                for d in response['response']['docs']
            ]
        except Exception:
            pass
    print(q, df, oae, op)
    raise RuntimeError('Reached max retries')


def trans_cand(i_, m_: str, c_: Work, ref_):
    return {
        'doi': c_.doi,
        'title': c_.title,
        'id': c_.id,
        'report': ref_['report'],
        'chapter': ref_['chapter_number'],
        'ipcc_ref': i_,
        'publication_year': c_.publication_year,
        'matched': m_
    }


ASCII = re.compile(r'[^A-Za-z0-9\- ]+')
DOI_REM = re.compile(r'(.*doi\.org/)*')
TOK = re.compile(r'(?u)\b\w\w+\b')
INT = re.compile(r'[^0-9]')


def s2i(s):
    if s is None:
        return None
    if type(s) is str:
        s = INT.sub('', s)
        if len(s) < 4:
            return None
        return int(s)
    return int(s)


def clean(d: dict[str, Any]) -> dict[str, Any]:
    return {
        k: v
        for k, v in d.items()
        if v is not None
    }


def strip_str(s: str) -> list[str]:
    return [
        token
        for token in TOK.findall(s.lower())
        if token not in {'the', 'a', 'is', 'an', 'for', 'in', 'to', 'that',
                         'then', 'than', 'there', 'so', 'or', 'of', 'on', 'by',
                         'and', 'from'}
    ]


def choose_candidate(i: int, ref: pd.Series, candidates: list[tuple[str, Work]]):
    yr1 = s2i(ref['year'])
    t1 = ref['title']
    tk1 = set(strip_str(t1)) if t1 is not None else set()

    def yr_in_range(yr2) -> bool:
        yr2 = s2i(yr2)
        if yr1 is not None and yr2 is not None:
            return abs(yr1 - yr2) <= 1
        return True  # unclear, default to true

    def title_overlap(t2) -> float:
        if t1 is not None and t2 is not None:
            # print(t1, '|', t2)
            tk2 = set(strip_str(t2))
            return len(tk1 & tk2) / len(tk1 | tk2)
        return .81  # unclear, default to threshold

    def pick(match) -> tuple[str, Work] | None:
        try:
            sample = [c for c in candidates if c[0] == match]
            if len(sample) > 0:
                # check year
                filtered = [c for c in sample if yr_in_range(c[1].publication_year)]
                if len(filtered) > 0:
                    ranked = [(title_overlap(c[1].title), c) for c in filtered]
                    ranked = sorted(ranked, key=lambda r: r[0])
                    # print(i, match, [r[0] for r in ranked])
                    if ranked[0][0] >= .8:
                        return ranked[0][1]
            return None
        except:
            print(candidates)
            raise NotImplementedError()

    def ret(pk: tuple[str, Work]) -> AcademicItemModel:
        ai = translate_work(pk[1])
        ipcc = clean(ref.to_dict())
        ai.meta = {
            'ipcc_ref': i,
            'source': f"AR6-{ipcc['report']}-Ch{ipcc['chapter_number']}",
            'matched': pk[0]
        }
        del ipcc['report']
        del ipcc['chapter_number']
        del ipcc['chapter_title']
        ai.meta['ipcc'] = ipcc
        return ai

    p = pick('doi')
    if p is not None:
        return ret(p)
    p = pick('exact_title')
    if p is not None:
        return ret(p)
    p = pick('strict_title')
    if p is not None:
        return ret(p)
    p = pick('fuzzy_title')
    if p is not None:
        return ret(p)
    return None


def get_candidates(tup: tuple[str, bool, str, tuple[tuple[str, int], pd.DataFrame]]):
    openalex_endpoint, override, path, ((rep, chap), df) = tup
    key = f'{rep}_Ch{chap:02d}'
    file_missing = Path(f'{path}/{key}_missed.feather')
    file_out = Path(f'{path}/{key}.jsonl')

    logger = logging.getLogger(key)

    if not override and (file_missing.exists() or file_out.exists()):
        logger.warning(f'Output for {key} already exists. Skipping!')
        return

    logger.info(f'[{key}] Finding candidates for {len(df):,} references...')
    candis = []
    missed = []
    it = 0
    for i, ref in df.iterrows():
        it += 1
        hit = False
        rcandis = []
        if ref['doi'] is not None or ref['DOI'] is not None:
            try:
                doi_ = ref.get('DOI') or ref.get('doi') or ''
                doi = DOI_REM.sub(doi_, '')
                if len(doi) == 0:
                    doi = doi_
                candidates = query(f'"https://doi.org/{doi.strip()}"', 'doi',
                                   limit=20, oae=openalex_endpoint)
                if len(candidates) > 0:
                    hit = True
                    rcandis += [('doi', c) for c in candidates]
            except Exception as e:
                logger.warning(key)
                logger.exception(e)
        if ref['title'] is not None and 10 < len(ref['title']) < 400:

            title = ASCII.sub(' ', ref["title"])
            ctitle = ' '.join(strip_str(title))
            try:
                # Exact title match
                candidates = query(f'"{title}"', 'title', limit=20, oae=openalex_endpoint)
                if len(candidates) > 0:
                    hit = True
                    rcandis += [('exact_title', c) for c in candidates]
            except Exception as e:
                logger.warning(key)
                logger.exception(e)
            try:
                # Strict title match
                candidates = query(ctitle, 'title', limit=20, oae=openalex_endpoint)
                if len(candidates) > 0:
                    hit = True
                    rcandis += [('strict_title', c) for c in candidates]
            except Exception as e:
                logger.warning(key)
                logger.exception(e)
            try:
                # Fuzzzy title match
                candidates = query(ctitle, 'title', op='OR', limit=20, oae=openalex_endpoint)
                if len(candidates) > 0:
                    hit = True
                    rcandis += [('fuzzy_title', c) for c in candidates]
            except Exception as e:
                logger.warning(key)
                logger.exception(e)
        try:
            cnd = choose_candidate(it, ref, rcandis)
            if cnd is None:
                hit = False
            else:
                candis.append(cnd)
        except Exception as e:
            logger.warning(key)
            logger.exception(e)
            hit = False

        if not hit:
            missed.append(ref.to_dict())

        if (it % 50) == 0 and it > 0:
            logger.debug(f'[{key}] {it:,}/{len(df):,}: {len(candis):,} candidates and {len(missed):,} not found.')

    logger.info(f'[{key}] Found {len(candis):,} candidates for {len(df):,} references, '
                f'but did not find anything for {len(missed):,} references...')

    df_ipcc_missing = pd.DataFrame(missed)
    feather.write_feather(df_ipcc_missing, str(file_missing))
    with open(file_out, 'w') as fout:
        [fout.write(AcademicItemModel.model_dump_json(c) + '\n') for c in candis]


def main(references: Path = Path('data/raw/IPCC/AR6_references.feather'),
         matches: Path = Path('data/ipcc_ar6'),
         openalex_endpoint: str = 'http://srv-mcc-apsis-rechner:8983/solr/openalex',
         logfile: Optional[Path] = None,
         parallelism: int = 1,
         override: bool = False,
         verbose: Verbosity = 2):
    logger = get_logger('refs', verbosity=verbose, logfile=logfile)
    logging.getLogger('httpx').setLevel(logging.WARNING)
    logging.getLogger('httpcore').setLevel(logging.WARNING)

    matches = matches.resolve()
    matches.mkdir(parents=True, exist_ok=True)

    logger.info(f'Trying to load list of references from {references}')
    df_ar6 = feather.read_feather(references)
    logger.debug(f'Loaded {df_ar6.shape} references.')

    with Pool(processes=parallelism) as pool:  # type: Pool
        pool.map(get_candidates, [(openalex_endpoint, override, str(matches), part)
                                  for part in df_ar6.groupby(['report', 'chapter_number'])])


if __name__ == '__main__':
    typer.run(main)
